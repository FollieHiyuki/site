# Personal blog

Me ranting (っ◔◡◔)っ

Powered by [lume](https://lume.land/) static site generator.

## TODO

- [ ] Toggle TOC (check <https://github.com/alex-shpak/hugo-book>)
- [ ] Add KaTeX support (maybe?)
- [ ] Add `Metas` and `Multilanguage` Lume plugins

## Ideas

These are things I've encountered and found interesting to talk about. I might
write a blog post about them at some point in the future (if I have the time to
do so).

- Build a multi-stack Pulumi monorepo with Bazel
- Writing a Pulumi project using Purescript
- Build Terraform monorepo with CDKTF and Nx
- Thoughts and a comparison of CDKTF and Pulumi
- Disapointing aspects of Hashicorp's Boundary
- How confusing it is to write Hashicorp's Vault policies
- Ansible variables management and the like (flatten/nested variables)

## 🌟 Credits

The website's was initially created based on
[hugo-flex](https://github.com/de-souza/hugo-flex).

The favicon bundle was generated with <https://realfavicongenerator.net> and
<https://maskable.app> using an icon from
[Kitty avatars pack](https://www.flaticon.com/packs/kitty-avatars-2).

## 📋 Licenses

The code powering this blog is licensed under the
[MIT](https://opensource.org/licenses/MIT) license. The articles and other
contents on the website are licensed under
[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0).
