export default function (title: string) {
  const slug = title.replace(/\s+/g, "-").toLowerCase();
  const now = Temporal.Now.plainDateTimeISO("UTC").toString();

  return {
    path: `/blog/${slug}.md`,
    content: {
      title: title,
      date: now.replace(/\.\d+/, "Z"),
    },
  };
}
