{
  description = "folliehiyuki's personal blog";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs =
    { nixpkgs, ... }:
    let
      eachSystem =
        f:
        nixpkgs.lib.genAttrs [
          "x86_64-linux"
          "aarch64-linux"
          "x86_64-darwin"
          "aarch64-darwin"
        ] (system: f nixpkgs.legacyPackages.${system});
    in
    {
      formatter = eachSystem (pkgs: pkgs.alejandra);

      devShells = eachSystem (
        pkgs: with pkgs; {
          default = mkShellNoCC {
            name = "site";
            meta.description = "Development shell for folliehiyuki's site";
            packages = with pkgs; [
              deno
              dprint
            ];
          };
        }
      );
    };
}
